from django import forms 
from .models import Pessoas, Numeros, Vendas
from django.forms import ModelForm
from django.contrib.postgres.fields import ArrayField
from django.forms import ModelChoiceField

class personForm(ModelForm):
    nome = forms.CharField(label='Nome', max_length=100)
    telefone = forms.CharField(label='Telefone', max_length=100)
    email = forms.EmailField(label='E-mail')
    cpf = forms.CharField(label='CPF', max_length=100)
    numero = ArrayField(forms.IntegerField(label='Numero(s)', required=False))

    class Meta:
        model = Pessoas
        fields = ['nome', 'telefone', 'email', 'cpf', 'numero']
        widgets = {'cpf': forms.TextInput(attrs={'data-mask':"000-000-0000"})}
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['nome'].widget.attrs.update({'class': 'style-input', 'id': 'nome'})
        self.fields['telefone'].widget.attrs.update({'class':'style-input mask-phone', 'id': 'telefone'})
        self.fields['email'].widget.attrs.update({'class': 'style-input', 'id': 'email'})
        self.fields['cpf'].widget.attrs.update({'class': 'style-input mask-cpf', 'id': 'cpf'})
        self.fields['numero'].widget.attrs.update({'class': 'style-input', 'id': 'numero', 'readonly':'readonly', 'help_text':'teste'})
        self.fields['numero'].help_text = 'Some text'

class VendasForm(ModelForm):
    pg = forms.CharField(label='Number', max_length=100)
    dono = ModelChoiceField(label='Pessoa', queryset=Pessoas.objects.all(), initial='')

    class Meta:
        model = Vendas
        fields = ['pg', 'dono']
