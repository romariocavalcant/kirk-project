from .models import Pessoas, Numeros, Vendas
from django.contrib import admin
from django.contrib import messages
from django.utils.translation import ngettext
from import_export import resources


class PessoasAdmin(admin.ModelAdmin):
    model = Pessoas
    list_filter = ['nome']
    list_display = ('cpf', 'nome', 'telefone', 'email')
    actions = ['pagou','naoPagou']
    
class NumerosAdmin(admin.ModelAdmin):
    model = Numeros
    list_display = ['numero', 'reservado', 'pagamento']
    actions = ['pagou','naoPagou', 'inserirReserva', 'removerReserva']


    def pagou(self, request, queryset):
        rmReserva = queryset.update(reservado=False)
        updated = queryset.update(pagamento=True)
        self.message_user(request, ngettext(
            '%d Pagamento Aprovado!',
            '%d Pagamento Aprovado!',
            updated,
        ) % updated, messages.SUCCESS)
    pagou.short_description = "Pagou"

    def naoPagou(self, request, queryset):
        rmReserva = queryset.update(reservado=False)
        updated = queryset.update(pagamento=False)
        self.message_user(request, ngettext(
            '%d Pagamento Removido!',
            '%d Pagamento Removido!',
            updated,
        ) % updated, messages.SUCCESS)
    naoPagou.short_description = "Não pagou"

    def inserirReserva(self, request, queryset):
        updated = queryset.update(reservado=True)
        self.message_user(request, ngettext(
            '%d Reserva Inserida!',
            '%d Reserva Inserida!',
            updated,
        ) % updated, messages.SUCCESS)
    inserirReserva.short_description = "Reservado"

    def removerReserva(self, request, queryset):
        updated = queryset.update(reservado=False)
        self.message_user(request, ngettext(
            '%d Reserva Removida!',
            '%d Reserva Removida!',
            updated,
        ) % updated, messages.SUCCESS)
    removerReserva.short_description = "Remover Reserva"

class VendasAdmin(admin.ModelAdmin):
    model = Vendas
    list_filter = ['dono']
    list_display = ['get_nome', 'get_numero', 'get_reservado', 'get_pagamento']
    actions = ['pagou','naoPagou', 'inserirReserva', 'removerReserva']


    def get_numero(self, obj):
        return "\n".join([str(a.numero) for a in obj.pg.all()])
    get_numero.short_description = 'Número(s)'

    def get_nome(self, obj):
        return "\n".join([str(a.nome) for a in obj.dono.all()])
    get_nome.short_description = 'Dono(a)'

    def get_pagamento(self, obj):
        return "\n".join([str(a.pagamento) for a in obj.pg.all()])
    get_pagamento.short_description = 'Pagamento(s)'

    def get_reservado(self, obj):
        return "\n".join([str(a.reservado) for a in obj.pg.all()])
    get_reservado.short_description = 'Reservado(s)'


    def pagou(self, request, queryset):
        for p in queryset:
            updated = p.pg.update(pagamento=True)
            updated = p.pg.update(reservado=False)
        self.message_user(request, ngettext(
            '%d Pagamento Aprovado!',
            '%d Pagamento Aprovado!',
            updated,
        ) % updated, messages.SUCCESS)
    pagou.short_description = "Pagou"

    def naoPagou(self, request, queryset):
        for p in queryset:
            updated = p.pg.update(pagamento=False)
            updated = p.pg.update(reservado=False)
        self.message_user(request, ngettext(
            '%d Pagamento Removido!',
            '%d Pagamento Removido!',
            updated,
        ) % updated, messages.SUCCESS)
    naoPagou.short_description = "Não pagou"

    def inserirReserva(self, request, queryset):
        for p in queryset:
            updated = p.pg.update(reservado=True)
            updated = p.pg.update(pagamento=False)
        self.message_user(request, ngettext(
            '%d Reserva Inserida!',
            '%d Reserva Inserida!',
            updated,
        ) % updated, messages.SUCCESS)
    inserirReserva.short_description = "Reservado"

    def removerReserva(self, request, queryset):
        for p in queryset:
            updated = p.pg.update(reservado=False)
            updated = p.pg.update(pagamento=False)
        self.message_user(request, ngettext(
            '%d Reserva Removida!',
            '%d Reserva Removida!',
            updated,
        ) % updated, messages.SUCCESS)
    removerReserva.short_description = "Remover Reserva"

admin.site.register(Pessoas, PessoasAdmin)
admin.site.register(Numeros, NumerosAdmin)
admin.site.register(Vendas, VendasAdmin)


