from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from .forms import personForm, VendasForm
import json
from .models import Pessoas, Numeros, Vendas
from django.urls import reverse


def index(request):
    # try:

        pessoa = personForm()        
        if request.method == 'POST':
            pessoa = personForm(request.POST)
            if pessoa.is_valid():
                num = pessoa.cleaned_data['numero']
                lista = num.split(',')
                for n in lista:
                    pessoa.save()
                return HttpResponseRedirect(reverse('success'))
            else:
                pessoa = personForm()
        
        # Verifica a quantidade de números vendidos
        # --------------------------------------------
        pagos = Numeros.objects.filter(pagamento=True)
        pg = 0
        for p in range(len(pagos)):
            pg = p + 1
        # --------------------------------------------

        #Verifica a quantidade de números não vendidos
        # --------------------------------------------
        livres = Numeros.objects.filter(pagamento=False, reservado=False)
        npg = 0
        for np in range(len(livres)):
            npg = np + 1
        # --------------------------------------------

        #Verifica a quantidade de números reservados
        # --------------------------------------------
        reservados = Numeros.objects.filter(reservado=True)
        reserv = 0
        for r in range(len(reservados)):
            reserv = r + 1
        # --------------------------------------------    

        vendas = Vendas.objects.all()
        for v in vendas:
            pass




        numeros = Numeros.objects.all()
        context = {
            'numeros':numeros,
            'pessoa':pessoa, 
            'vendas':vendas, 
            'pago':pg,
            'livre':npg,
            'reservado': reserv
            }
        return render(request, 'index.html', context)


    # except Exception as error:
    #     return HttpResponse("Ocorreu um erro ao tentar renderizar a página!")



def success(request):
    # try:

        return render(request, 'success.html')

    # except Exception as error:
    #     return HttpResponse("Ocorreu um erro ao tentar renderizar a página!")