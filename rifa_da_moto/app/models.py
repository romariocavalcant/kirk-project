from django.db import models
from django.forms import ModelForm
from psycopg2.extras import NumericRange
from django.contrib.postgres.fields import ArrayField
from django.core.validators import MinValueValidator, MaxValueValidator

class Pessoas(models.Model):
    pessoa_id = models.IntegerField(primary_key=True)
    cpf = models.CharField(verbose_name='CPF', max_length=100, name='cpf', primary_key=False)
    nome = models.CharField(verbose_name='Nome', max_length=100, name='nome')
    telefone =  models.CharField(verbose_name='Telefone', max_length=100, name='telefone')
    email = models.EmailField(verbose_name='E-mail', max_length=100, name='email')
    number = models.CharField(verbose_name='Número(s)', max_length=2000, name='numero')
    # number = ArrayField(verbose_name='Numero(s)', base_field=models.IntegerField(), default=list, size=None, name='numero')

    def __str__(self):
        return str(self.nome)

    class Meta:
        ordering = ["nome"]
        verbose_name_plural = "Clientes"

class Numeros(models.Model):
    numero = models.IntegerField(primary_key=True, validators=[MinValueValidator(1), MaxValueValidator(1000)])
    pagamento = models.BooleanField(verbose_name='Pago', default=False)
    reservado = models.BooleanField(verbose_name='Reservado', default=False)

    def __str__(self):
        return str(self.numero)

    class Meta:
        ordering = ["numero"]
        verbose_name_plural = "Números"

class Vendas(models.Model):
    pg = models.ManyToManyField(Numeros, related_name='pg', verbose_name='Número(s)')
    dono = models.ManyToManyField(Pessoas, related_name='dono', verbose_name='Dono(a)')

    def __str__(self):
        return "\n".join([str(a.numero) for a in self.pg.all()])

    def __str__(self):
        return "\n".join([str(a.nome) for a in self.dono.all()])

    class Meta:
        verbose_name_plural = "Vendas"

    

   


